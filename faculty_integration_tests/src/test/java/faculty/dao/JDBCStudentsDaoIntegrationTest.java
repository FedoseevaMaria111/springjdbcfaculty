package faculty.dao;

import com.ra.course.janus.faculty.configuration.SpringConfig;
import com.ra.course.janus.faculty.entity.Student;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = SpringConfig.class)
@TestPropertySource("classpath:test_config.properties")
@Sql("classpath:scripts/create_faculty.sql")

 class JDBCStudentsDaoIntegrationTest {
    @Autowired
    private NamedJDBCStudentDao studentDao;
    private static final Student TEST_STUDENT = new Student(1L, " student", "234");

    @Test
    public void whenInsertStudentReturnId() {
        final Student createdStudent = studentDao.insert(TEST_STUDENT);
        final Student somestudent = studentDao.insert(TEST_STUDENT);
        assertNotSame(createdStudent.getId(), somestudent.getId());
    }

    @Test
    public void updateWhenExists() {
        Student somestudent = studentDao.insert(TEST_STUDENT);
        Student updatedstudent = somestudent;
        updatedstudent.setCode("234");

        assertTrue(studentDao.update(updatedstudent));
    }

    @Test
    public void deleteFromDB() {
        final Long id = studentDao.insert(TEST_STUDENT).getId();
        studentDao.delete(id);
        assertThrows(EmptyResultDataAccessException.class, () -> studentDao.selectById(id));

    }
@Test
    public void findById() {

        final Student savedStudent = studentDao.insert(TEST_STUDENT);

        final Student foundStudent = studentDao.selectById(savedStudent.getId());

        assertEquals(savedStudent, foundStudent);

    }


    @Test
    public void findAll() {

        final Student savedStudent = studentDao.insert(TEST_STUDENT);

        final Student foundStudent = studentDao.selectById(savedStudent.getId());

        assertEquals(savedStudent, foundStudent);

    }

}
