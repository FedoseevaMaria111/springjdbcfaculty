package faculty.dao;

import com.ra.course.janus.faculty.configuration.ConnectionUtil;
import com.ra.course.janus.faculty.entity.Course;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ConnectionUtil.class})
public class JDBCCourseDAOIntegrationTest {
    @Autowired
    private GenericDAO<Course> genericDAO;

    private Course course = new Course(1L, "Roma");

    @Test
    public void insertCourseTest() {
        Course created = genericDAO.insert(course);

        assertEquals(created, genericDAO.selectById(created.getId()));
    }

    @Test
    public void selectCourseTest() {
        for (Course course : genericDAO.select())
            genericDAO.delete(course.getId());

        Course testCourse = genericDAO.insert(course);
        List<Course> expected = Collections.singletonList(testCourse);
        List<Course> actual = genericDAO.select();

        assertIterableEquals(expected, actual);
    }

    @Test
    public void selectByIdCourseTest() {
        Course created = genericDAO.insert(course);
        Course gotten = genericDAO.selectById(created.getId());

        assertEquals(created, gotten);
    }

    @Test
    public void updateCourseTest() {
        Course created = genericDAO.insert(course);

        created.setTeacher("Max");

        genericDAO.update(created);

        Course updated = genericDAO.selectById(created.getId());

        assertEquals(created, updated);
    }

    @Test
    public void deleteCourseTest() {
        Course created = genericDAO.insert(course);

        genericDAO.delete(created.getId());

        Course gotten = genericDAO.selectById(created.getId());

        assertNull(gotten);
    }
}
