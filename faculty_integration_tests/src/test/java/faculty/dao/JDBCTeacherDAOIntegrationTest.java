package faculty.dao;

import com.ra.course.janus.faculty.configuration.ConnectionUtil;
import com.ra.course.janus.faculty.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ConnectionUtil.class})
public class JDBCTeacherDAOIntegrationTest {
    @Autowired
    private GenericDAO<Teacher> genericDAO;

    private Teacher teacher = new Teacher(1L, "Roma", "JavaEE");

    @Test
    public void insertTeacherTest() {
        Teacher created = genericDAO.insert(teacher);

        assertEquals(created, genericDAO.selectById(created.getId()));
    }

    @Test
    public void selectTeacherTest() {
        for (Teacher teacher : genericDAO.select())
            genericDAO.delete(teacher.getId());

        Teacher testTeacher = genericDAO.insert(teacher);
        List<Teacher> expected = Collections.singletonList(testTeacher);
        List<Teacher> actual = genericDAO.select();

        assertIterableEquals(expected, actual);
    }

    @Test
    public void selectByIdTeacherTest() {
        Teacher created = genericDAO.insert(teacher);
        Teacher gotten = genericDAO.selectById(created.getId());

        assertEquals(created, gotten);
    }

    @Test
    public void updateTeacherTest() {
        Teacher created = genericDAO.insert(teacher);

        created.setName("Max");
        created.setCourse("PHP");

        genericDAO.update(created);

        Teacher updated = genericDAO.selectById(created.getId());

        assertEquals(created, updated);
    }

    @Test
    public void deleteTeacherTest() {
        Teacher created = genericDAO.insert(teacher);

        genericDAO.delete(created.getId());

        Teacher gotten = genericDAO.selectById(created.getId());

        assertNull(gotten);
    }
}
