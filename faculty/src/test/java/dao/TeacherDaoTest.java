package dao;

import com.ra.course.janus.faculty.entity.Teacher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TeacherDaoTest {
    private static final String INSERT_TEACHER = "insert into teacher (name, course) values (?, ?)";
    private static final String SELECT_TEACHER = "select * from teacher";
    private static final String SELECT_BY_ID = "select * from teacher where id = ?";
    private static final String UPDATE_TEACHER = "update teacher set name = ?, course = ? where id = ?";
    private static final String DELETE_TEACHER = "delete from teacher where id = ?";

    private static final long ID = 1L;

    private static final Teacher TEACHER = new Teacher(ID, "Roma", "JavaEE");

    private JdbcTemplate jdbcTemplate = mock(JdbcTemplate.class);
    private Connection connection = mock(Connection.class);
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);

    private GenericDAO<Teacher> genericDAO = new JDBCTeacherDAO(jdbcTemplate);

    private Map<String, Object> getTestMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", TEACHER.getId());
        map.put("name", TEACHER.getName());
        map.put("course", TEACHER.getCourse());
        return map;
    }

    @Test
    public void whenCalledInsertShouldReturnTeacher() throws Exception {
        when(connection.prepareStatement(INSERT_TEACHER, Statement.RETURN_GENERATED_KEYS)).thenReturn(preparedStatement);
        when(jdbcTemplate.update(any(PreparedStatementCreator.class), any(KeyHolder.class))).thenAnswer(
                (Answer) invocation -> {
                    Object[] args = invocation.getArguments();
                    PreparedStatementCreator creator = (PreparedStatementCreator) args[0];
                    creator.createPreparedStatement(connection);

                    KeyHolder keyHolder = (KeyHolder) args[1];
                    Map<String, Object> map = new HashMap<>();
                    map.put("key", 1L);
                    keyHolder.getKeyList().add(map);
                    return 1;
                }
        );

        Teacher teacher = genericDAO.insert(TEACHER);
        assertEquals(TEACHER, teacher);
    }

    @Test
    public void whenCalledSelectThenReturnNonEmptyList() {
        List<Map<String, Object>> rows = new ArrayList<>();
        rows.add(getTestMap());
        when(jdbcTemplate.queryForList(SELECT_TEACHER)).thenReturn(rows);

        List<Teacher> list = genericDAO.select();
        assertFalse(list.isEmpty());
    }

    @Test
    public void whenCalledSelectByIdThenReturnIt() {
        when(jdbcTemplate.queryForObject(eq(SELECT_BY_ID), any(BeanPropertyRowMapper.class), eq(ID))).thenReturn(TEACHER);

        Teacher teacher = genericDAO.selectById(ID);

        assertEquals(ID, teacher.getId());
    }

    @Test
    public void whenCalledSelectByIdThenReturnNull() {
        when(jdbcTemplate.queryForObject(eq(SELECT_BY_ID), any(BeanPropertyRowMapper.class), eq(ID))).thenThrow(new EmptyResultDataAccessException(1));

        Teacher teacher = genericDAO.selectById(ID);

        assertNull(teacher);
    }

    @Test
    public void whenCalledUpdateThenReturnTrue() {
        when(jdbcTemplate.update(eq(UPDATE_TEACHER), any(PreparedStatementSetter.class))).thenAnswer((Answer) invocation -> {
            Object[] args = invocation.getArguments();
            PreparedStatementSetter setter = (PreparedStatementSetter) args[1];
            setter.setValues(preparedStatement);
            return 1;
        });

        boolean isUpdated = genericDAO.update(TEACHER);

        assertTrue(isUpdated);
    }

    @Test
    public void whenCalledUpdateThenReturnFalse() {
        when(jdbcTemplate.update(eq(UPDATE_TEACHER), any(PreparedStatementSetter.class))).thenReturn(0);

        boolean isUpdated = genericDAO.update(TEACHER);

        assertFalse(isUpdated);
    }

    @Test
    public void whenCalledDeleteThenReturnTrue() {
        when(jdbcTemplate.update(DELETE_TEACHER, ID)).thenReturn(1);

        boolean isDeleted = genericDAO.delete(ID);

        assertTrue(isDeleted);
    }

    @Test
    public void whenCalledDeleteThenReturnFalse() {
        when(jdbcTemplate.update(DELETE_TEACHER, ID)).thenReturn(0);

        boolean isDeleted = genericDAO.delete(ID);

        assertFalse(isDeleted);
}
