package dao;

import com.ra.course.janus.faculty.dao.NamedJDBCCourseDao;
import com.ra.course.janus.faculty.entity.Course;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CourseDaoTest {
    private static final String INSERT_COURSE = "insert into course (teacher) values (?)";
    private static final String SELECT_COURSE = "select * from course";
    private static final String SELECT_BY_ID = "select * from course where id = ?";
    private static final String UPDATE_COURSE = "update course set teacher = ? where id = ?";
    private static final String DELETE_COURSE = "delete from course where id = ?";

    private static final long ID = 1L;

    private static final Course COURSE = new Course(ID, "Roma");

    private JdbcTemplate jdbcTemplate = mock(JdbcTemplate.class);
    private Connection connection = mock(Connection.class);
    private PreparedStatement preparedStatement = mock(PreparedStatement.class);

    private GenericDAO<Course> genericDAO = new JDBCCourseDAO(jdbcTemplate);

    private Map<String, Object> getTestMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", COURSE.getId());
        map.put("teacher", COURSE.getTeacher());
        return map;
    }

    @Test
    public void whenCalledInsertShouldReturnTeacher() throws Exception {
        when(connection.prepareStatement(INSERT_COURSE, Statement.RETURN_GENERATED_KEYS)).thenReturn(preparedStatement);
        when(jdbcTemplate.update(any(PreparedStatementCreator.class), any(KeyHolder.class))).thenAnswer(
                (Answer) invocation -> {
                    Object[] args = invocation.getArguments();
                    PreparedStatementCreator creator = (PreparedStatementCreator) args[0];
                    creator.createPreparedStatement(connection);

                    KeyHolder keyHolder = (KeyHolder) args[1];
                    Map<String, Object> map = new HashMap<>();
                    map.put("key", 1L);
                    keyHolder.getKeyList().add(map);
                    return 1;
                }
        );

        Course course = genericDAO.insert(COURSE);
        assertEquals(COURSE, course);
    }

    @Test
    public void whenCalledSelectThenReturnNonEmptyList() {
        List<Map<String, Object>> rows = new ArrayList<>();
        rows.add(getTestMap());
        when(jdbcTemplate.queryForList(SELECT_COURSE)).thenReturn(rows);

        List<Course> list = genericDAO.select();
        assertFalse(list.isEmpty());
    }

    @Test
    public void whenCalledSelectByIdThenReturnIt() {
        when(jdbcTemplate.queryForObject(eq(SELECT_BY_ID), any(BeanPropertyRowMapper.class), eq(ID))).thenReturn(COURSE);

        Course course = genericDAO.selectById(ID);

        assertEquals(ID, course.getId());
    }

    @Test
    public void whenCalledSelectByIdThenReturnNull() {
        when(jdbcTemplate.queryForObject(eq(SELECT_BY_ID), any(BeanPropertyRowMapper.class), eq(ID))).thenThrow(new EmptyResultDataAccessException(1));

        Course course = genericDAO.selectById(ID);

        assertNull(course);
    }

    @Test
    public void whenCalledUpdateThenReturnTrue() {
        when(jdbcTemplate.update(eq(UPDATE_COURSE), any(PreparedStatementSetter.class))).thenAnswer((Answer) invocation -> {
            Object[] args = invocation.getArguments();
            PreparedStatementSetter setter = (PreparedStatementSetter) args[1];
            setter.setValues(preparedStatement);
            return 1;
        });

        boolean isUpdated = genericDAO.update(COURSE);

        assertTrue(isUpdated);
    }

    @Test
    public void whenCalledUpdateThenReturnFalse() {
        when(jdbcTemplate.update(eq(UPDATE_COURSE), any(PreparedStatementSetter.class))).thenReturn(0);

        boolean isUpdated = genericDAO.update(COURSE);

        assertFalse(isUpdated);
    }

    @Test
    public void whenCalledDeleteThenReturnTrue() {
        when(jdbcTemplate.update(DELETE_COURSE, ID)).thenReturn(1);

        boolean isDeleted = genericDAO.delete(ID);

        assertTrue(isDeleted);
    }

    @Test
    public void whenCalledDeleteThenReturnFalse() {
        when(jdbcTemplate.update(DELETE_COURSE, ID)).thenReturn(0);

        boolean isDeleted = genericDAO.delete(ID);

        assertFalse(isDeleted);
    }}

