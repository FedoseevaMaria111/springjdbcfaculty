package faculty.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
    public class Student {
        private long id;
        private String code;
        private String description;

        public Student(final Student student) {
            this(student.getId(), student.getCode(), student.getDescription());
        }

    }




