package faculty.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Course  {
    private long id;
    private String code;
    private String description;

    public Course(long id,Course course) {
        this(id, course.getCode(), course.getDescription());
    }

}
