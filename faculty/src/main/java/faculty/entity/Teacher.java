package faculty.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Teacher {
    private long id;
    private String name;
    private String course;

    public Teacher(final Teacher teacher) {
        this(teacher.getId(), teacher.getName(), teacher.getCourse());
    }
}
