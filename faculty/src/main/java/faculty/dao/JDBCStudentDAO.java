package faculty.dao;

import com.ra.course.janus.faculty.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
@Component
public class JDBCStudentDAO implements GenericDAO<Student> {
    private static final String INSERT_STUDENT = "insert into student (mark, course) values (?, ?)";
    private static final String SELECT_STUDENT = "select * from student";
    private static final String SELECT_BY_ID = "select * from student where id = ?";
    private static final String UPDATE_STUDENT = "update student set mark = ?, course = ? where id = ?";
    private static final String DELETE_STUDENT = "delete from student where id = ?";

    private transient final JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCStudentDAO(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Student insert(final Student student) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STUDENT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, student.getMark());
            preparedStatement.setString(2, student.getCourse());
            return preparedStatement;
        }, keyHolder);
        final long id = keyHolder.getKey().longValue();
        return new Student(id, student);
    }

    @Override
    public List<Student> select() {
        final List<Map<String, Object>> list = jdbcTemplate.queryForList(SELECT_STUDENT);
        return list.stream().map(rows -> {
            final Student student = new Student();
            student.setId((long) rows.get("id"));
            student.setMark((int) rows.get("mark"));
            student.setCourse((String) rows.get("course"));
            return student;
        }).collect(Collectors.toList());
    }

    @Override
    public Student selectById(final long id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_BY_ID, BeanPropertyRowMapper.newInstance(Student.class), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public boolean update(final Student student) {
        final int rows = jdbcTemplate.update(UPDATE_STUDENT, preparedStatement -> {
            preparedStatement.setInt(1, student.getMark());
            preparedStatement.setString(2, student.getCourse());
            preparedStatement.setLong(3, student.getId());
        });
        return rows != 0;
    }

    @Override
    public boolean delete(final long id) {
        final int rows = jdbcTemplate.update(DELETE_STUDENT, id);
        return rows != 0;
    }
}
