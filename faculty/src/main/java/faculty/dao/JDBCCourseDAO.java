package faculty.dao;

import com.ra.course.janus.faculty.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
@Component
public class JDBCCourseDAO implements GenericDAO<Course> {
    private static final String INSERT_COURSE = "insert into course (teacher) values (?)";
    private static final String SELECT_COURSE = "select * from course";
    private static final String SELECT_BY_ID = "select * from course where id = ?";
    private static final String UPDATE_COURSE = "update course set teacher = ? where id = ?";
    private static final String DELETE_COURSE = "delete from course where id = ?";

    private transient final JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCCourseDAO(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Course insert(final Course course) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_COURSE, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, course.getTeacher());
            return preparedStatement;
        }, keyHolder);
        final long id = keyHolder.getKey().longValue();
        return new Course(id, course);
    }

    @Override
    public List<Course> select() {
        final List<Map<String, Object>> list = jdbcTemplate.queryForList(SELECT_COURSE);
        return list.stream().map(rows -> {
            final Course course = new Course();
            course.setId((long) rows.get("id"));
            course.setTeacher((String) rows.get("teacher"));
            return course;
        }).collect(Collectors.toList());
    }

    @Override
    public Course selectById(final long id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_BY_ID, BeanPropertyRowMapper.newInstance(Course.class), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public boolean update(final Course course) {
        final int rows = jdbcTemplate.update(UPDATE_COURSE, preparedStatement -> {
            preparedStatement.setString(1, course.getTeacher());
            preparedStatement.setLong(2, course.getId());
        });
        return rows != 0;
    }

    @Override
    public boolean delete(final long id) {
        final int rows = jdbcTemplate.update(DELETE_COURSE, id);
        return rows != 0;
    }
}
