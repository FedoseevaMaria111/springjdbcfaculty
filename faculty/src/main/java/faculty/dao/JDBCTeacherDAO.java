package faculty.dao;

import com.ra.course.janus.faculty.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
@Component
public class JDBCTeacherDAO implements GenericDAO<Teacher> {
    private static final String INSERT_TEACHER = "insert into teacher (name, course) values (?, ?)";
    private static final String SELECT_TEACHER = "select * from teacher";
    private static final String SELECT_BY_ID = "select * from teacher where id = ?";
    private static final String UPDATE_TEACHER = "update teacher set name = ?, course = ? where id = ?";
    private static final String DELETE_TEACHER = "delete from teacher where id = ?";

    private transient final JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCTeacherDAO(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Teacher insert(final Teacher teacher) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEACHER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, teacher.getName());
            preparedStatement.setString(2, teacher.getCourse());
            return preparedStatement;
        }, keyHolder);
        final long id = keyHolder.getKey().longValue();
        return new Teacher(id, teacher);
    }

    @Override
    public List<Teacher> select() {
        final List<Map<String, Object>> list = jdbcTemplate.queryForList(SELECT_TEACHER);
        return list.stream().map(rows -> {
            final Teacher teacher = new Teacher();
            teacher.setId((long) rows.get("id"));
            teacher.setName((String) rows.get("name"));
            teacher.setCourse((String) rows.get("course"));
            return teacher;
        }).collect(Collectors.toList());
    }

    @Override
    public Teacher selectById(final long id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_BY_ID, BeanPropertyRowMapper.newInstance(Teacher.class), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public boolean update(final Teacher teacher) {
        final int rows = jdbcTemplate.update(UPDATE_TEACHER, preparedStatement -> {
            preparedStatement.setString(1, teacher.getName());
            preparedStatement.setString(2, teacher.getCourse());
            preparedStatement.setLong(3, teacher.getId());
        });
        return rows != 0;
    }

    @Override
    public boolean delete(final long id) {
        final int rows = jdbcTemplate.update(DELETE_TEACHER, id);
        return rows != 0;
    }
}
