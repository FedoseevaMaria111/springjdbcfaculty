package faculty.dao;

import java.util.List;

public interface GenericDao <T>{

    T insert(T teacher);

    List<T> select();

    T selectById(long id);
    boolean update(T teacher);
    boolean delete(long id);
}
